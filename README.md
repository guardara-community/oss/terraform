# GUARDARA Terraform Configuration

This Terraform configuration is a simple example of how you can set up GUARDARA. The configuration deploys GUARDARA in a dedicated VPC that:

 * Has an AWS hosted zone with a domain name registered
 * Exposes GUARDARA on the internet on a subdomain of the primary domain
 * Sets up a certificate so GUARDARA can be accessed securely over HTTPS

If you already have an AWS environment that is managed using Terraform, you will likely just want to take some modules (e.g. instance) and integrate into your existing configuration.

If you only wish to evaluate GUARDARA it's likely the fastest and easiest is to use the Installer Bundle to deploy it on your local machine or a VM. (It is a lot easier to start a script compared to tweaking Terraform configs.)

While this Terraform configuration sets up a reasonably simple environment for GUARDARA, you can further simplify the setup if you wish by Dropping the ALB and NLB and exposing the GUARDARA ports directly to the internet. In this case, you'd want to use something like [letsencrypt](https://letsencrypt.org) and update the instance's setup to issue the certificate for GUARDARA automatically. See the [Custom Frontend Certificate](https://guardara-community.gitlab.io/documentation/docs/deployment#custom-frontend-certificate) section of the documentation for more details. Of course, you would have to adjust the security group accordingly as well.

# Setup

After cloning the repository (or making any changes to the modules) issue the `terraform init` command to initialize the modules.

# Configuration

This Terraform configuration sets up a subdomain where GUARDARA is going to be hosted. Accordingly, the `hosted_zone_id` of the primary domain must be provided in the configuration. It also expects a domain name (`domain`) registered and assigned to the hosted zone. If this is not your preferred setup, you can tweak and change the Terraform files any way you like.

The configuration options are documented in the configuration file. (`config.yml`)

# Applying Changes / Refreshing

Issue the `terraform apply` command to build the environment or apply changes.
