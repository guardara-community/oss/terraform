locals {
  config = yamldecode(file("${path.module}/config.yml"))
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
  required_version = ">= 1.3.4"
}

provider "aws" {
  profile = "default"
  region  = local.config.region
}

resource "aws_key_pair" "instance-deploy-key" {
  key_name   = "instance-deploy-key"
  public_key = local.config.instance_deploy_public_key
}

# -----------------------------------------------------------------------------
# VPC
# -----------------------------------------------------------------------------

module "guardara" {
  source                         = "./modules/guardara"
  config                         = local.config
}
