variable "vpc_id" {}
variable "subnet_id" {}
variable "fake_subnet_id" {}
variable "manager_instance_id" {}
variable "certificate_arn" {}
