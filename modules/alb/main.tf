module "allow_https" {
  source = "../security/https_in"
  vpc_id = var.vpc_id
  sources = [ "0.0.0.0/0" ]
  port = 443
}

resource "aws_lb" "manager" {
  internal           = false
  load_balancer_type = "application"
  security_groups    = [module.allow_https.id]
  subnets            = [var.subnet_id, var.fake_subnet_id]
}

resource "aws_lb_target_group" "manager" {
  vpc_id                = var.vpc_id
  port                  = 8443
  protocol              = "HTTPS"
}

resource "aws_lb_target_group_attachment" "manager" {
  target_group_arn = aws_lb_target_group.manager.arn
  target_id        = var.manager_instance_id
  port             = 8443
  depends_on = [
    aws_lb_target_group.manager
  ]
}

resource "aws_lb_listener" "manager" {
  load_balancer_arn = aws_lb.manager.arn
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-TLS-1-2-2017-01"
  certificate_arn   = var.certificate_arn

  default_action {
    type            = "forward"
    target_group_arn = aws_lb_target_group.manager.arn
  }
  depends_on = [
    aws_lb.manager,
    aws_lb_target_group.manager
  ]
}
