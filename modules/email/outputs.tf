output "smtp_username" {
  value = module.smtp_credentials.smtp_username
}

output "smtp_password" {
  value = module.smtp_credentials.smtp_password
}

output "smtp_server" {
    value = "email-smtp.${var.region}.amazonaws.com"
}

output "smtp_tls" {
    value = false
}

output "smtp_port" {
    value = 587
}
