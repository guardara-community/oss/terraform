module "smtp_credentials" {
    source = "./ses_credentials"
}

# -----------------------------------------------------------------------------
# SES DKIM Verification
# -----------------------------------------------------------------------------

resource "aws_ses_domain_dkim" "main" {
  domain = var.domain
}

resource "aws_route53_record" "dkim" {
  count = 3
  zone_id = var.dns_zone_id
  name = format(
    "%s._domainkey.%s",
    element(aws_ses_domain_dkim.main.dkim_tokens, count.index),
    var.domain,
  )
  type    = "CNAME"
  ttl     = "600"
  records = ["${element(aws_ses_domain_dkim.main.dkim_tokens, count.index)}.dkim.amazonses.com"]
}

# -----------------------------------------------------------------------------
# SES MAIL FROM Domain
# -----------------------------------------------------------------------------

resource "aws_ses_domain_mail_from" "main" {
  domain           = var.domain
  mail_from_domain = "notifications.${var.domain}"
}

# -----------------------------------------------------------------------------
# SPF
# -----------------------------------------------------------------------------

resource "aws_route53_record" "spf_mail_from" {
  zone_id = var.dns_zone_id
  name    = aws_ses_domain_mail_from.main.mail_from_domain
  type    = "TXT"
  ttl     = "600"
  records = ["v=spf1 include:amazonses.com ~all"]
}

# -----------------------------------------------------------------------------
# Sending MX Record
# -----------------------------------------------------------------------------

resource "aws_route53_record" "mx_send_mail_from" {
  zone_id = var.dns_zone_id
  name    = aws_ses_domain_mail_from.main.mail_from_domain
  type    = "MX"
  ttl     = "600"
  records = ["10 feedback-smtp.${var.region}.amazonses.com"]
}

# -----------------------------------------------------------------------------
# DMARC TXT Record
# -----------------------------------------------------------------------------

resource "aws_route53_record" "txt_dmarc" {
  zone_id = var.dns_zone_id
  name    = "_dmarc.${var.domain}"
  type    = "TXT"
  ttl     = "600"
  records = ["v=DMARC1; p=none; rua=mailto:${var.dmarc_reporting_address};"]
}
