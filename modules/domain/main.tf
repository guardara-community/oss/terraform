resource "aws_ses_domain_identity" "primary" {
  domain = var.domain
}

resource "aws_route53_record" "domain_verification_verification" {
  zone_id = var.hosted_zone_id
  name    = "_amazonses.${aws_ses_domain_identity.primary.id}"
  type    = "TXT"
  ttl     = "600"
  records = [aws_ses_domain_identity.primary.verification_token]
}

resource "aws_ses_domain_identity_verification" "primary" {
  domain = aws_ses_domain_identity.primary.id
  depends_on = [aws_route53_record.domain_verification_verification]
}
