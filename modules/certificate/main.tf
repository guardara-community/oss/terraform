resource "aws_acm_certificate" "wildcard_cert" {
  domain_name               = var.domain
  subject_alternative_names = ["*.${var.domain}"]
  validation_method         = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "cert_validations" {
  zone_id = var.hosted_zone_id
  name = element(aws_acm_certificate.wildcard_cert.domain_validation_options.*.resource_record_name, 0)
  type = element(aws_acm_certificate.wildcard_cert.domain_validation_options.*.resource_record_type, 0)
  records = [element(aws_acm_certificate.wildcard_cert.domain_validation_options.*.resource_record_value, 0)]
  ttl = 60
}

resource "aws_acm_certificate_validation" "wildcard_cert" {
  certificate_arn         = aws_acm_certificate.wildcard_cert.arn
  validation_record_fqdns = aws_route53_record.cert_validations.*.fqdn
  depends_on = [
    aws_route53_record.cert_validations
  ]
}
