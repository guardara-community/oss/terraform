locals {
  private_ip        = cidrhost(var.subnet_cidr, 5)
  content_ssdh_config = templatefile("${path.module}/assets/sshd_config.tpl", {})
  content_cloudinit = templatefile("${path.module}/assets/guardara_init.yml.tpl", {
    hostname        = var.host_name
    public_domain   = var.domain
    admin_email     = var.admin_email
    admin_password  = var.admin_password
    smtp_server     = var.smtp_details.smtp_server
    smtp_port       = var.smtp_details.smtp_port
    smtp_tls        = var.smtp_details.smtp_tls
    smtp_username   = var.smtp_details.smtp_username
    smtp_password   = var.smtp_details.smtp_password
  })
}

module "allow_all_out" {
  source = "../security/allow_all_out"
  vpc_id = var.vpc_id
}

module "allow_ssh" {
  source = "../security/ssh_in"
  vpc_id = var.vpc_id
}

module "allow_https_manager" {
  source = "../security/https_in"
  vpc_id = var.vpc_id
  sources = [ "0.0.0.0/0" ]
  port = 8443
}

module "allow_https_engines" {
  source = "../security/https_in"
  vpc_id = var.vpc_id
  sources = [ "0.0.0.0/0" ]
  port = 8444
}

resource "aws_volume_attachment" "database" {
  device_name = "/dev/sdd"
  instance_id = aws_instance.manager.id
  volume_id   = var.ebs_id
  stop_instance_before_detaching = true
}

resource "aws_network_interface" "network" {
  subnet_id       = var.subnet_id
  private_ips     = [local.private_ip]
  security_groups = [
    module.allow_ssh.id,
    module.allow_https_manager.id,
    module.allow_https_engines.id,
    module.allow_all_out.id
  ]
}

data "cloudinit_config" "server_config" {
  gzip          = true
  base64_encode = true
  part {
    content_type = "text/cloud-config"
    content = templatefile("${path.module}/assets/init.yml.tpl", {
      db_ebs_device_name: "xvdd",
      content_sshd_config: base64encode(local.content_ssdh_config),
      content_cloudinit: base64encode(local.content_cloudinit),
      public_domain: var.domain,
      public_hostname: var.host_name,
      private_ip: local.private_ip,
      installer: var.installer_url
    })
  }
}

resource "aws_instance" "manager" {
  ami                   = "ami-0fdf70ed5c34c5f52"
  instance_type         = "t2.medium"
  key_name              = "instance-deploy-key"
  availability_zone     = var.availability_zone

  network_interface {
    network_interface_id = aws_network_interface.network.id
    device_index         = 0
  }

  user_data = data.cloudinit_config.server_config.rendered

  tags = {
    Name = "Manager"
  }
}

locals {
  out = {
    instance_id = aws_instance.manager.id
  }
}
