Port 50022
Protocol 2
AllowUsers keyman, ubuntu
ListenAddress 0.0.0.0
UsePrivilegeSeparation sandbox
HostKey /etc/ssh/ssh_host_ed25519_key
HostKey /etc/ssh/ssh_host_rsa_key
KexAlgorithms curve25519-sha256@libssh.org,ecdh-sha2-nistp521,ecdh-sha2-nistp384,ecdh-sha2-nistp256
Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes256-ctr,aes192-ctr
MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,hmac-sha2-512,hmac-sha2-256
SyslogFacility AUTH
LogLevel VERBOSE
LoginGraceTime 2m
PasswordAuthentication no
PermitRootLogin no
#StrictModes yes
MaxAuthTries 2
MaxSessions 5
PubkeyAuthentication yes
AuthorizedKeysFile	.ssh/authorized_keys
HostbasedAuthentication no
IgnoreRhosts yes
PermitEmptyPasswords no
ChallengeResponseAuthentication no
UsePAM yes
AllowAgentForwarding yes
AllowTcpForwarding yes
X11Forwarding no
PrintMotd no
PrintLastLog yes
TCPKeepAlive no
ClientAliveInterval 300
ClientAliveCountMax 2
PermitUserEnvironment no
UseDNS no
Compression no
AcceptEnv LANG LC_*
Subsystem	sftp	/usr/lib/openssh/sftp-server
