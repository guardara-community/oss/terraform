#cloud-config
package_update: true
write_files: 
  - path: /etc/ssh/sshd_config
    encoding: b64
    owner: root:root
    permissions: "0644"
    content: |
      ${content_sshd_config}
  - path: /root/cloud-init.yml
    encoding: b64
    owner: root:root
    permissions: "0644"
    content: |
      ${content_cloudinit}
  - path: /etc/hosts
    content: | 
       ${private_ip}  ${public_hostname}.${public_domain} ${public_hostname}
    append: true
  - path: /etc/environment
    content: | 
       ${private_ip}  ${public_hostname}.${public_domain} ${public_hostname}
    append: true
  - path: /etc/hostname
    content: | 
       ${public_hostname}.${public_domain}
runcmd:
  # Make sure the EBS is mounted
  - export EBS_DEVICE=${db_ebs_device_name}
  - export EBS_FORMATTED=$(file -s /dev/$EBS_DEVICE | grep 'XFS filesystem' | wc -l)
  - export EBS_UUID=$(lsblk -o +UUID | grep $EBS_DEVICE | awk '{ print $8 }')
  - if [ $EBS_FORMATTED -eq 0 ]; then mkfs -t xfs /dev/$EBS_DEVICE; fi
  - mkdir /data
  - mount /dev/$EBS_DEVICE /data
  - cp /etc/fstab /etc/fstab.orig
  - echo "UUID=$EBS_UUID  /data  xfs  defaults,nofail  0  2" >> etc/fstab
  # Setup hostname
  - hostname "${public_hostname}.${public_domain}"
  - hostnamectl set-hostname "${public_hostname}.${public_domain}"
  - service ssh restart
  # Have to upgrade/install here because we have to work around an Ubuntu cloud-init bug 
  - rm /var/lib/dpkg/info/install-info.postinst
  # openssh-server causing issues as it ignores all possible attempts to prevent 
  # the interactive crap about the config change, so we will skip upgrading it.
  - apt-mark hold openssh-server
  - export DEBIAN_FRONTEND=noninteractive | apt-get upgrade -y
  # Big ,,|, to the Ubuntu package maintainers too
  - export DEBIAN_FRONTEND=noninteractive | apt-get install -yq -o Dpkg::Options::=--force-confold -o Dpkg::Options::=--force-confdef --allow-downgrades --allow-remove-essential --allow-change-held-packages docker docker.io docker-compose curl zip
  # We are bypassing AWS enforced ,,|, 16KB cloud-init file limit using git
  - curl ${installer} -o ./installer.zip
  - unzip ./installer.zip
  - rm ./installer.zip
  - mv ./install.sh /root/
  - chmod 700 /root/install.sh
  - cd /root ; export DEBIAN_FRONTEND=noninteractive | ./install.sh -i
