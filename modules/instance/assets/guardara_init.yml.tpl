setup:
  fqdn: ${hostname}.${public_domain}
  admin_email: ${admin_email}
  admin_password: ${admin_password}
  smtp_server: ${smtp_server}
  smtp_tls: ${smtp_tls}
  smtp_port: ${smtp_port}
  smtp_username: ${smtp_username}
  smtp_password: ${smtp_password}
