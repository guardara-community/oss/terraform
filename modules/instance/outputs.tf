output "id" {
  value = aws_instance.manager.id
}

output "private_ip" {
  value = local.private_ip
}
