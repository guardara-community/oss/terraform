variable "vpc_id" {}
variable "availability_zone" {}
variable "domain" {}
variable "host_name" {}
variable "subnet_id" {}
variable "subnet_cidr" {}

variable "admin_email" {}
variable "admin_password" {}
variable "installer_url" {}

variable "smtp_details" {}
variable "ebs_id" {}
