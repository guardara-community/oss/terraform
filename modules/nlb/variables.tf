variable "hosted_zone_id" {}
variable "vpc_id" {}
variable "subnet_id" {}
variable "host_name" {}
variable "manager_instance_id" {}
variable "alb_arn" {}
