resource "aws_eip" "nlb" {
  vpc = true
}

resource "aws_lb" "network" {
  internal           = false
  load_balancer_type = "network"

  subnet_mapping {
    subnet_id     = var.subnet_id
    allocation_id = aws_eip.nlb.id
  }
}

resource "aws_lb_target_group" "alb" {
  vpc_id                = var.vpc_id
  target_type           = "alb"
  port                  = 443
  protocol              = "TCP"
}

resource "aws_lb_target_group_attachment" "alb" {
  target_group_arn = aws_lb_target_group.alb.arn
  target_id        = var.alb_arn
  port             = 443
}

resource "aws_lb_listener" "network_manager" {
  load_balancer_arn = aws_lb.network.arn
  port              = 443
  protocol          = "TCP"
  default_action {
    type            = "forward"
    target_group_arn = aws_lb_target_group.alb.arn
  }
}

resource "aws_lb_target_group" "engines" {
  vpc_id                = var.vpc_id
  port                  = 8444
  protocol              = "TCP"
}

resource "aws_lb_target_group_attachment" "engines" {
  target_group_arn = aws_lb_target_group.engines.arn
  target_id        = var.manager_instance_id
  port             = 8444
}

resource "aws_lb_listener" "network_engines" {
  load_balancer_arn = aws_lb.network.arn
  port              = 8444
  protocol          = "TCP"
  default_action {
    type            = "forward"
    target_group_arn = aws_lb_target_group.engines.arn
  }
}

resource "aws_lb_target_group" "ssh" {
  vpc_id                = var.vpc_id
  port                  = 50022
  protocol              = "TCP"
}

resource "aws_lb_target_group_attachment" "ssh" {
  target_group_arn = aws_lb_target_group.ssh.arn
  target_id        = var.manager_instance_id
  port             = 50022
}

resource "aws_lb_listener" "network_ssh" {
  load_balancer_arn = aws_lb.network.arn
  port              = 50022
  protocol          = "TCP"
  default_action {
    type            = "forward"
    target_group_arn = aws_lb_target_group.ssh.arn
  }
}

# -----------------------------------------------------------------------------
# DNS
# -----------------------------------------------------------------------------

resource "aws_route53_record" "cname_route53_record" {
  zone_id = var.hosted_zone_id
  name    = "${var.host_name}"
  type    = "CNAME"
  ttl     = "60"
  records = [aws_lb.network.dns_name]
}
