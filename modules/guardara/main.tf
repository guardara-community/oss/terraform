module "vpc" {
  source    = "../vpc"
  cidr      = var.config.vpc_cidr
}

module "domain" {
  source          = "../domain"
  domain          = var.config.domain
  hosted_zone_id  = var.config.hosted_zone_id
}

resource "time_sleep" "wait_for_domain" {
  depends_on = [module.domain]

  create_duration = "30s"
}

module "certificate" {
  source          = "../certificate"
  domain          = var.config.domain
  hosted_zone_id  = var.config.hosted_zone_id
}

module "email" {
  source                  = "../email"
  domain                  = var.config.domain
  dns_zone_id             = var.config.hosted_zone_id
  region                  = var.config.region
  dmarc_reporting_address = var.config.dmarc_reporting_address
  depends_on = [
    time_sleep.wait_for_domain
  ]
}

module "internet_gateway" {
  source  = "../gateway"
  vpc_id  = module.vpc.id
}

module "eip" {
  source  = "../eip"
}

module "subnets" {
  source              = "../subnets"
  vpc_id              = module.vpc.id
  vpc_cidr            = var.config.vpc_cidr
  availability_zone   = var.config.availability_zone
  fake_subnet_az      = var.config.fake_alb_zone
}

module "nat" {
  source            = "../nat"
  eip_id            = module.eip.id
  public_subnet_id  = module.subnets.public_subnet_id
}

module "routes" {
  source              = "../routes"
  main_route_table_id = module.vpc.main_route_table_id
  internet_gateway_id = module.internet_gateway.id
  nat_gateway_id      = module.nat.gateway_id
  vpc_id              = module.vpc.id
  private_subnet_id   = module.subnets.private_subnet_id
}

module "ebs" {
  source = "../ebs"
  availability_zone = var.config.availability_zone
}

module "instance" {
  source = "../instance"
  vpc_id = module.vpc.id
  availability_zone = var.config.availability_zone
  domain = var.config.domain
  host_name = var.config.sub_domain
  subnet_id = module.subnets.private_subnet_id
  subnet_cidr = module.subnets.private_subnet_cidr
  admin_email = var.config.guardara.admin_email
  admin_password = var.config.guardara.admin_password
  installer_url = var.config.guardara.installer
  smtp_details = module.email
  ebs_id = module.ebs.id
}

resource "time_sleep" "wait_for_certificate" {
  depends_on = [module.certificate]

  create_duration = "30s"
}

module "alb" {
  source              = "../alb"
  vpc_id              = module.vpc.id
  subnet_id           = module.subnets.public_subnet_id
  fake_subnet_id      = module.subnets.fake_subnet_id
  manager_instance_id = module.instance.id
  certificate_arn     = module.certificate.certificate_arn
  depends_on = [
    time_sleep.wait_for_certificate
  ]
}

module "nlb" {
  source              = "../nlb"
  hosted_zone_id      = var.config.hosted_zone_id
  vpc_id              = module.vpc.id
  host_name           = var.config.sub_domain
  subnet_id           = module.subnets.public_subnet_id
  manager_instance_id = module.instance.id
  alb_arn             = module.alb.arn
  depends_on = [
    module.alb
  ]
}
