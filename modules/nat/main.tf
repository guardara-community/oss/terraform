resource "aws_nat_gateway" "gw" {
  allocation_id = var.eip_id
  subnet_id     = var.public_subnet_id
}
