
resource "aws_ebs_volume" "storage" {
  availability_zone = var.availability_zone
  size              = 10
}
