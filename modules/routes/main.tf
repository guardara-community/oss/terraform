resource "aws_route" "route_internet" {
  route_table_id            = var.main_route_table_id
  destination_cidr_block    = "0.0.0.0/0"
  gateway_id                = var.internet_gateway_id
}

resource "aws_route_table" "private" {
  vpc_id = var.vpc_id
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = var.nat_gateway_id
  }
}

resource "aws_route_table_association" "private" {
  subnet_id = var.private_subnet_id
  route_table_id = aws_route_table.private.id
}
