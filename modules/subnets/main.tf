resource "aws_subnet" "private" {
  vpc_id            = var.vpc_id
  availability_zone = var.availability_zone
  cidr_block        = cidrsubnet(var.vpc_cidr, 1, 1)
}

resource "aws_subnet" "public" {
  vpc_id            = var.vpc_id
  availability_zone = var.availability_zone
  cidr_block        = cidrsubnet(var.vpc_cidr, 2, 1)
}

resource "aws_subnet" "fake" {
  vpc_id            = var.vpc_id
  availability_zone = var.fake_subnet_az
  cidr_block        = cidrsubnet(var.vpc_cidr, 3, 1)
}
