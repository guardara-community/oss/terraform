output "private_subnet_id" {
  value = aws_subnet.private.id
}

output "private_subnet_cidr" {
  value = aws_subnet.private.cidr_block
}

output "public_subnet_id" {
  value = aws_subnet.public.id
}

output "public_subnet_cidr" {
  value = aws_subnet.public.cidr_block
}

output "fake_subnet_id" {
  value = aws_subnet.fake.id
}

output "fake_subnet_cidr" {
  value = aws_subnet.fake.cidr_block
}
