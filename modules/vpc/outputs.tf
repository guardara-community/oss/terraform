output "id" {
  description = "The VPC ID"
  value = module.vpc.vpc_id
}

output "main_route_table_id" {
  value = module.vpc.vpc_main_route_table_id
}
