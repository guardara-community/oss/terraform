module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name                  = "guardara-vpc"
  cidr                  = var.cidr
  enable_dns_hostnames  = true
  enable_ipv6           = false
  enable_nat_gateway    = false
  single_nat_gateway    = true
}
